import React from 'react';
import SignIn from './components/signIn/SignIn';
import Profile from './components/profile/Profile';
import './App.css';
import Provider from './components/context/Provider';
import NotFound from './components/NotFound';
import AddEditNote from "./components/notes/AddEditNote";
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
function App() {
  return (
    <Router>
      <Provider>
        <div>
          <Switch>
            <Route exact path="/" component={SignIn} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/profile/AddNote" component={()=><AddEditNote note="Add Note"/>}/>
            <Route exact path="/profile/editNote/:id" component={()=><AddEditNote note="Edit Note"/>}/>
            <Route path="*" component={NotFound} />
            
          </Switch>
        </div>
      </Provider>
    </Router>
  );
}

export default App;
