import * as actionTypes from './actionTypes';
import {getNotes} from "../components/axios/axios"
import {addNotes}from "../components/axios/axios"
import {deleteNotes} from "../components/axios/axios"
import {editNotes} from "../components/axios/axios"

export const fetchNotes=()=>{
    return dispatch=>{
       let query={userId:localStorage.id}
       let headers={Authorization:localStorage.token}
        getNotes({query,headers})
        .then(response=>{console.log('notes details',response.data.data[0].docs);
        return dispatch({
            type: actionTypes.NOTE_DATA,
            noteData: response.data.data[0].docs
          });
        })
        .catch(err => {
          console.log(err);
        });
    };
    
};
export const addNote = (payLoad, header) => {
    return dispatch => {
      addNotes(payLoad, header)
         .then(response=>{console.log('add notes',response.data.data);
        return dispatch({
            type: actionTypes.ADD_NOTE,
            arrayObj: response.data.data
          });
        })
        .catch(error => {

          console.log(error);
        });
    };
  };
export const deleteNote = data => {
return dispatch => {
    deleteNotes(data)
    .then(response => {
        dispatch({
        type: actionTypes.REMOVE_NOTE,
        deleteObj:response
        });
    })
    .catch(err => {
        console.log(err);
    })
    ;};
  };
export const editNote = (noteId,params, header) => {
return dispatch => {
editNotes(noteId,params, header)
.then(response => {
    dispatch({
    type: actionTypes.EDIT_NOTE,
    editObj:response
    });
})
  .catch(err => {

    });
  };
};