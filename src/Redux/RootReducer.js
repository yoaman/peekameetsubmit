import * as actionTypes from './actionTypes'

const initialState = {
  notes: [],
  fetchData: false
};

function RootReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.NOTE_DATA: {
      let updated = { ...state }
      updated.notes = action.noteData;
      return updated;
    }

    case actionTypes.ADD_NOTE: {
      let updated = { ...state };
      updated.notes = [...updated.notes, ...action.arrayObj];
      return updated
    }

    case actionTypes.REMOVE_NOTE: {
      let updated = { ...state };
      let dataId = action.deleteObj.data.data[0]._id
      let i = updated.notes.findIndex(x => x._id === dataId);
      updated.notes = updated.notes.slice(0, i).concat(updated.notes.slice(i + 1, updated.notes.length))
      return updated
    }
    case actionTypes.EDIT_NOTE: {
      let data = action.editObj.data.data[0];
      let updated = { ...state };
      let notes = updated.notes.map((el) => {
        if (el._id === data._id) {
          return data;
        }
        else {
          return el;
        }
      })
      updated.notes = notes;
      return updated;
    }
    default:
      return state;
  };
}
export default RootReducer;