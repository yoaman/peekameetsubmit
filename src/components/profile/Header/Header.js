import React,{Component} from 'react';
import img from '../../signIn/header/peekameet.png';
import img1 from './Images/group-copy-3.png';
import img2 from './Images/credit-card-scan.png';
import img3 from './Images/group-copy-4.png';
import img4 from './Images/group-copy.png';
import img5 from './Images/credit-card-scan.png';
import img6 from './Images/group-copy-12.png';
import './Header.css';
import {Redirect} from 'react-router-dom';
import img7 from '../Images/menu@2x.png';

class Header extends Component{
    state={
        login:true
    }
    SignOut=()=>{
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        this.setState({login:false})
    }
    render(){
        if(this.state.login){
        }
        else{
            return <Redirect to="/"></Redirect>
        }
        return(
            <div className="Rectangle">
                <img src={img} className="peekameet"></img>
                    <div className="SubHeader">
                        <div className="SubHeaderElements">
                            <img src={img1} className="Group-Copy-3"></img>
                            <span className="Home">Home</span>
                            <hr className="underline3"/>
                        </div>
                       
                        <div className="SubHeaderElements">
                            
                            <img src={img2} className="Group-Copy-4"></img>
                            <span className="Contacts">Contacts</span>
                        </div>
                        <div className="SubHeaderElements">
                        
                            <img src={img3} className="Group-Copy"></img> 
                            <span className="Messages">Messages</span>
                        </div>
                        <div className="SubHeaderElements">
                            
                            <img src={img4} className="credit-card-scan"></img>
                            <span className="Scan">Scan</span>
                        </div>
                        <div className="SubHeaderElements">
                            
                            <img src={img5} className="Group-Copy-12"></img>
                            <span className="The-Movement">The Movement</span>
                        </div>
                        <div className="SubHeaderElements">
                            
                            <img src={img6} className="Group-Copy-11"></img>
                            <span className="Notifications-Copy">Notifications</span>
                        </div>
                        <div>
                            <img src={img7} className="Menu"/>
                        </div>
                        {/* <div>
                            <img src={img7} className="menu"></img>
                        </div> */}
                    </div>
                    <div >
                        <button className="SignOut" onClick={()=>this.SignOut()} >SignOut</button>
                    </div>
            </div>
        
        )
    }
}
  
export default Header;