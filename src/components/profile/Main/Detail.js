import React, { Component } from 'react';
import './Main.css';
import img7 from '../Images/group-26.png';
import img8 from '../Images/group-27.png';
import img9 from '../Images/shape.png';

const Detail=(props)=>{
    console.log(props.data.user.bio)
    const bio=props.data.user.bio;
    const Details = [
        { industry: props.data.user.industry },
        { organisationGroups: props.data.user.organisationGroups },
        { interestActivities: props.data.user.interestActivities },
        { alumni: props.data.user.alumni },
        { languages: props.data.user.languages }

    ];
    const arr = Details.map(element => (
        element[Object.keys(element)].map(element2 =>
            (element2))))
    const arr2 = Details.map(element => (
        Object.keys(element)[0]))
    return(

        <div>
            <hr className="underline"/>
            
            
            <div className="UserDetailsBox">
                       <div className="bio"><p className="quotes">"</p>{bio}<p className="quotes">"</p></div> 
                
                {arr2.map((element, index) => {
                    let className = "UserDetailsElement" + index;
                    let className2 = "UserDetailsText" + index;
                    return (
                        <div>
                            
                            <span className="UserDetialsLabel">{element}</span>
                            <div className="UserTextGroup">
                                {arr[index].map(element2 => (
                                    <div className={className2}>
                                        <div className={className}>
                                            <span className="UserDetailsTextInside">{element2}</span>
                                        </div>
                                    </div>
                                ))}
                                <br />
                            </div>
                            <br />
                        </div>
                    )
                })}
            </div>
            <div className="groupContactInfo">
                <div className="groupInfo">
                    <img src={img7} className="infoImage" />
                    <p className="infoText">{props.data.user.email}</p>
                </div>
                <div className="groupInfo">
                    <img src={img9} className="infoImage" />
                    <p className="infoText"></p>
                </div>
                <div className="groupInfo">
                    <img src={img8} className="infoImage" />
                    <p className="infoText">{props.data.user.businessPhone}</p>
                </div>

            </div>
            <div className="Address">
                <p className="Business-Address">Business Address</p>
                <p className="-Sycamore-Dr">{props.data.user.businessAddress}</p>

            </div>
        </div>
        
    )

}


export default Detail;
