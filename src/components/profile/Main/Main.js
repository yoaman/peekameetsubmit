import React, { Component } from 'react';
import './Main.css';
import { myContext } from '../../context/myContext';
import img3 from '../Images/group-copy.png';
import img4 from '../Images/group-29.png';
import img5 from '../Images/contact-copy-2.png';
import img6 from '../Images/play-circle-filled.png';
import Detail from './Detail';
import Notes from '../../notes/Notes';




class Main extends Component {
    state={
        stateVariable:false
    }
    clickHandler(type){
        console.log("hi")
        if(type==='details'){
            this.setState({stateVariable:false})
        }
        else{
            this.setState({stateVariable:true})
        }


    }

    render() {
        return (
            <div className="MainProfile">
                {console.log('IN App')}
                <myContext.Consumer>
                    {((context) => {

                        if (Object.keys(context.user).length !== 0) {
                            console.log(context.user);
                        
                            return (
                                <div>
                                    <img src={context.user.additionalImages[0].url} className="Group" />
                                    <div className="ProfilePhoto">
                                        <img src={context.user.profileImage.url} className="UserPhoto" />
                                    </div>
                                    <div className="rightOfProfile">
                                        <img src={img6} />
                                        <img src={img4} />
                                    </div>
                                    <div className="BasicInfo">
                                        <p className="Robert-Smith">{context.user.firstName} {context.user.lastName}</p>
                                        <p className="CEO">{context.user.jobTitle}</p>
                                        <p className="Gold-Brands">{context.user.company}</p>
                                    </div>
                                    <div className="messageContact">
                                        <span className="RectangleMessageContact">
                                            <img className="MessageImage" src={img3} />
                                            <p className="Message">Message</p>
                                        </span>
                                        <span className="RectangleMessageContact">
                                            <img className="ContactImage" src={img5} />
                                            <p className="SaveContact">Save Contact</p>
                                        </span>
                                    </div>
                                    <br />
                                   
                                    <div className="DivDetailsNotes">
                                        <ul className="DetailsNotes">
                                            <li className="details" onClick={()=>this.clickHandler('details')}>Details</li>
                                            <li className="Notes" onClick={()=>this.clickHandler('notes')}>Notes</li>
                                        </ul>
                                    </div>
                                    
                                    {this.state.stateVariable ? <Notes data={context}/>: <Detail data={context} />}
                                   
                                   
                                </div>

                            )
                        }
                    })}
                </myContext.Consumer>
            </div>
        )
    }
}
export default Main;