import React,{Component} from 'react';
import Header from './Header/Header';
import Main from './Main/Main';
import Footer from '../signIn/footer/Footer'; 
import {myContext} from '../context/myContext';

export default class Profile extends Component{
    static contextType=myContext;
    componentWillMount(){
        if(localStorage.token && localStorage.userId){
        const data=[
            {userId:localStorage.getItem('userId')},
            {Authorization:localStorage.getItem('token')}
        ];
        this.context.userProfile(data[0],data[1]);
        }
        else{
            this.props.history.push('/');
        }

    }
    render(){
        return(
            <div>
                <Header/>
                <Main/>
                <Footer/>
            </div>

            )
            
        }
    }
