import React from 'react';
import './Main.css';
import img from './group-18.jpg';
import Link from './Links';
import Form from './Form';
const Main=()=>{
    const Links=['Freelancer','Job-Seeker','Enterpreneur','Mompreneur','Internship-Seeker','Environmental-Change-Maker'];
    return(
        <div className="Main">
            <div>
                <img src={img} alt="not displayed" className="Group-18"/>
            </div>
            <div>
                <div className="MainSubHeader">
                    <span className="PEEKaMEET-lets-you-n ">PEEKaMEET</span>
                    <span className=".text-style-1"> lets you network more effectively to achieve your business and career goals</span>
                </div>
                <div className="Lists">
                    {Links.map((link,index)=>{
                        const Rectangle="Rectangle"+index;
                        return(
                            <div className="List" key={index}>
                                <Link classOuter={Rectangle} class={link} value={link}/>
                            </div>
                            )
                            }
                        )
                    }
                    
                </div>
                <div className="Build-and-manage-you">
                    <p>Build and manage your network with PEEKaMEET</p>
                </div>
                <div>
                    <Form/>
                </div>
            </div>
        </div>
        
        
    )
}
export default Main;