import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from 'react-loader-spinner';
import React from 'react';
import './Main.css';
const Spinner=()=>{
    return (
        <div className="spinner">

        <Loader
            type="Puff"
            color="#00BFFF"
            height={25}
            width={25}
        />
        </div>
    )
}
export default Spinner;