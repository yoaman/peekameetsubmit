import React,{Component} from 'react';
import Header from './header/Header';
import Footer from './footer/Footer';
import './SignIn.css';
import Main from './main/Main';

// import Profile from '../Profile/Profile'


class SignIn extends Component{
    componentWillMount(){

        if(localStorage.token){
            this.props.history.push('/Profile');
        }
    }
    render(){
    return(
        <div className="SignIn">
            {/* <Profile/> */}
            <Header/>
            <Main/>
            <Footer/>
        
        </div>

    )
    }
}
export default SignIn;