import React from 'react';
import img from './peekameet.png'
import './Header.css';

const Header = () => {
    return (
        <div className="Header">
            <div>
                <span ><img src={img} alt="can not displayed" className="PEEKaMEET" /></span>

            </div>
            <div>
                <div className="SubHeader">
                    <div className="RectangleSignIn">
                        <span className="Sign-In">SignIn</span>
                    </div>
                    <div className="RectangleSignUp">
                        <span className="Sign-Up">SignUp</span>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Header;