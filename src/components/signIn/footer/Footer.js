import React from 'react';
import fb from './images/facebook.png'
import insta from './images/instagram-fill.png';
import twit from'./images/twitter-fill.png';
import youtube from './images/youtube-fill.png';
import './Footer.css';
const Footer=()=>{
    const FooterContent=['FAQs','Terms & Conditions','Privacy Policy','About Us','Press','Contact Us','Perks','Blog'];
    return(
        <div className="RectangleFooter">
            <span className="PEEKaMEET2020">PEEKaMEET©2020</span>
            <span className="Follow-Us">Follow us:</span>
            <div className="Logos">
                
                <img className="insta" src={insta}></img>
                <img className="youtube" src={youtube}></img>
                <img className="fb" src={fb}></img>
                <img className="twit" src={twit}></img>
            </div>
            <div className="Contents">
               {FooterContent.map((content)=>{
                    return(
                        <div>
                            <a href="#" className="Content">{content}</a>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
export default Footer;