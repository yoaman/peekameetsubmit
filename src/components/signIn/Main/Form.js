import React, { Component } from 'react';
import './Main.css';
import {MyContext} from '../../context/myContext';
import Spinner from './Spinner';

class Form extends Component {
    
    static contextType=MyContext;
    state = {
        email:null,
        password:"",
        status:null,
        loading:false,
        emailValid:true,
        passwordValid:true,
        canClick:true,
        requestIsComplete:false
    }
    ChangeHandler = (event) => {
        
        const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        const passwordPattern = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
        if (event.target.name === 'email') {
            this.setState({emailValid:false})
             this.setState({email:event.target.value});
            if (emailPattern.test(event.target.value)){
                this.setState({emailValid:true})
            } 
        }
        else {
            this.setState({passwordValid:false})
            this.setState({ password:event.target.value })
            if (passwordPattern.test(event.target.value)){
                this.setState({passwordValid:true})
            } 
        }
        this.setState({status:null})
        if(emailPattern.test(this.state.email) && passwordPattern.test(this.state.password)){
            this.setState({canClick:false})
        }
    }
    
    submit = (event) => {
        this.setState({loading:true})
        event.preventDefault();
        this.context.signIn({ email: this.state.email, password: this.state.password }).then((result) => {
            this.setState({status:result,loading:false,requestIsComplete:true});
         })
    }
    render() {
        
        return (
            <div className="Form">
                <form onSubmit={(event) => this.submit(event)}>
                <div className="Field1">
                <div className="Email">Email</div>
                <br />
             
                <input type="text" name="email" className="Rectangle-Copy-6"
                    onChange={(event) => this.ChangeHandler(event)}></input>
                {this.state.emailValid? null:<p className="validateEmail">Invalid Format of email</p>}
            </div>
            <br />
            <br/>
            <div className="Field2">
                <div className="Password">Password</div>
               
                <br />
                <input type="password" name="password" className="Rectangle-Copy-6"
                    onChange={(event) => this.ChangeHandler(event)}></input>
                {this.state.passwordValid?null: <p className="validateEmail">Invalid Format of password</p>}
                
            </div>
            <br />{
                console.log("hiiii",this.state.requestIsComplete)
            }
            <button type="submit" className="RectangleSignIn" disabled={this.state.canClick && !this.state.requestIsComplete}>
                <span className="Sign-In">Sign-In
                <div className="spinner">{this.state.loading ? <Spinner/>:null}</div>
                </span>
            </button>
            
            <br/>
            <br />
            <br />
            <div className="checkRememberForgot">
                <span className="CheckRemember">
                    <input type="checkbox"></input>
                    <span className="Remember-Me">Remember Me</span>
                </span>
                <span className="Forgot-Password">Forgot Password</span>
            </div>
            <br />
            <div className="Dont-have-an-account">
                <span>Dont-have-an-account?</span>
                <span className="Sign-Up">Sign_Up</span>
            </div>
          </form>
        </div>

        )
    }
}
export default Form;











