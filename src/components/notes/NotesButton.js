import React from "react";

const NotesButton=()=>{
    let btn=['Add note','Edit note','Delete note']
    return(
        <div>
          {
              btn.map((item=>{
                return  <button className="notes_buttons">{item}</button>
              }))
          }
        </div>
    )
}
export default NotesButton;