import React, { Component } from "react";
import {connect} from 'react-redux';
import "./notes.css";
import GetNotes from "./GetNotes";
import {fetchNotes} from "../../Redux/Action"

class Notes extends Component{
    componentDidMount(){
        this.props.noteData()
    }
    render(){
        return(
            <div className="NotesStruct">
                <hr className="underline2"/>
                <div className="notes_wrapper">
                   <GetNotes/>
                </div>
            </div>
           
        )
    }
}
const mapStateToProps=state=>{
    return{ notes:state.notes}
}
const mapDispatchToProps=dispatch=>{
    return{
        noteData:()=>dispatch(fetchNotes())
    };
};

export default connect(mapStateToProps,mapDispatchToProps) (Notes);