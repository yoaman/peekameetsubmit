import React, { Component } from "react";
import { addNote } from "../../Redux/Action";
import { editNote } from "../../Redux/Action";
import "./notes.css";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import Header from '../profile/Header/Header';
import Footer from '../signIn/footer/Footer';
import { myContext } from "../context/myContext";
class AddEditNote extends Component {
    state = {
        notes: "",
        date: "",
        time: "",

    }
    componentWillMount() {
        if(this.props.note==='Edit Note'){
            let reqNote;
            this.props.notes.map((el)=>{
                if(el._id===this.props.match.params.id){
                    reqNote=el;
                }
            })
            var date=new Date(reqNote.dateTime*1000);
            var year=date.getUTCFullYear();
            var month=date.getUTCMonth()+1;
            let day = date.getUTCDate();
            let hours = date.getUTCHours();
            let  minutes = date.getUTCMinutes();
            if(month<10){
                month="0"+month;
            }
            if(hours<10){
                hours="0"+hours;
            }
            if(minutes<10){
                minutes="0"+minutes;
            }
            date=year+'-'+month+'-'+day;
            let time=hours+':'+minutes;
            console.log(reqNote);
            this.setState({notes:reqNote.noteText,time:time,date:date})
            console.log(reqNote.noteText)
            if (!(localStorage.token && localStorage.userId)) {
                this.props.history.push('/');
            }
    }
    else{

        if (!(localStorage.token && localStorage.userId)) {
            this.props.history.push('/');
        }
    }
}
   
    changeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value,

        })

    }
    submitHandler = event => {
        if (this.state.notes === "" || this.state.date === "" || this.state.time === "") {
            alert("Please fill all the fields")
        }
        let notes = this.state.notes
        let date = this.state.date
        let time = this.state.time
        let dateArray = date.split('-');
        let timeArray = time.split(':');
        let dateTimeStamp = (new Date(Date.UTC(dateArray[0], dateArray[1] - 1, dateArray[2], timeArray[0], timeArray[1]))).getTime() / 1000;
        if(this.props.note==='Edit Note'){
            let noteId=this.props.match.params.id;
            let params={
                "noteText":this.state.notes,
                "dateTime":dateTimeStamp
            }
            let headers={
                Authorization:localStorage.token
            }
            this.props.editANote(noteId,params,{headers})
            this.props.history.push({
                pathname:'/profile'
            });
            
        }
        else{
            let body = {
                "createdFor": localStorage.userId,
                "type": 'note',
                "noteText": this.state.notes,
                "dateTime": dateTimeStamp
            }
            let headers = {
                Authorization: localStorage.token
            }
            this.props.addANote(body, { headers })
            this.props.history.push({ pathname: '/profile'})
    
        }
    }

       
    render() {
        return (
            <div>
                <Header />
                <div className="outter_notes">

                    <div className="notes_wrappers">
                        <div className="add_note_heading">
                            <h3> {this.props.note}</h3>
                        </div>
                        <myContext.Consumer>
                            {(context =>
                                <div>
                                    <img className="notesImg" src={context.user.profileImage.url} />
                                    <span className="name">{context.user.firstName + ' ' + context.user.lastName}</span>

                                </div>
                            )}
                        </myContext.Consumer>
                        <br />
                        <div className="notesField">
                            <div>
                                <span className="follow_up_date">Follow up date</span>
                                <span className="follow_up_time">Time</span>
                            </div>

                            <div>
                                <input className="date" type="date" name="date" onChange={this.changeHandler}
                                value={this.state.date}></input>
                                <input type="time" name="time" onChange={this.changeHandler}
                                value={this.state.time}></input>
                            </div>
                            <br />
                            <div>
                                <span className="write_notes"> Notes</span>
                            </div>
                            <br />
                            <div>
                                {
                                    console.log(this.state.note)
                                }
                                <input type="textarea" className="notes_text"
                                    name="notes" placeholder="Write Notes Here" onChange={this.changeHandler}
                                    defaultValue={this.state.notes} ></input>
                            </div>
                            <br />
                            <div className="cancelSubmit">
                                <Link className="cancel" to="../"> <span className="span">Cancel</span></Link>
                                <Link className="submit" to="../" onClick={this.submitHandler}><span className="span">Save</span></Link>
                            </div>

                        </div>

                    </div>
                </div>
                <Footer />
            </div>
        )

    }
}
const mapStateToProps = state => {
    return { notes: state.notes }
}

const mapDispatchToProps = dispatch => {
    return {
        addANote: (body, headers) => dispatch(addNote(body, headers)),
        editANote: (noteId, params, headers) => dispatch(editNote(noteId, params, headers))

    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddEditNote));