import { connect } from "react-redux";
import React from "react";
import "./notes.css";
import img from './images/shape.png';
import { Link } from "react-router-dom";
import { deleteNote } from "../../Redux/Action";
import { withRouter } from "react-router-dom";
const GetNotes = (props) => {
    let data = [];
    props.lists.map((item) => {
        data.push(item);
    })
    const deleteHandler = (event, id) => {
        let params = {
            noteId: id
        }
        let headers = {
            Authorization: localStorage.token
        }
        props.deleteANote({ params, headers });
    }
    const editHandler = (event, el) => {
        props.history.push({
            pathname: '/Profile/editNote/' + el._id,
            data: {
                noteText: el.noteText
            }
        });
    }
    return (
        <div>
            <div className="notes_main_div" >
                <Link className="addnote" to="/Profile/AddNote"><img src={img} />Add Notes</Link><br></br>
                {data.map((item => {
                    let theDate = new Date(item.dateTime * 1000)
                    let date = theDate.toGMTString();
                    date = new Date(date).toUTCString();
                    date = date.split(' ').slice(0, 4).join(' ');
                    return <div className="notes_div" >
                        <span className="get_notes">{item.noteText}</span>
                        <br />
                        <span className="notes_date">{date}</span>
                        <div className="btn_notes">

                            <button className="deleteNote" onClick={(event) => deleteHandler(event, item._id)}>Delete Notes</button>
                            <button className="editNote" onClick={(event) => editHandler(event, item)}>Edit Notes</button>
                        </div>
                        </div>
                }))
                }
            </div>
        </div>
    )
}
const mapStateToProps = state => ({
    lists: state.notes
})
const mapDispatchToProps = dispatch => {
    return {
        deleteANote: (data) => dispatch(deleteNote(data))

    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GetNotes));