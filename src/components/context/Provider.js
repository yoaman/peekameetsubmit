import { myContext } from './myContext';
import React, { Component } from 'react';
import { userAuth, userLogin } from '../axios/axios';
import { withRouter } from 'react-router-dom';

class Provider extends Component {
    state = {
        user: {}
    }
    render() {
        return (
            <myContext.Provider value={{
                user: this.state.user,
                signIn: async (userInfo) => {
                    const user = await userLogin(userInfo);
                    if (user) {
                        localStorage.setItem('userId', user.userId);
                        localStorage.setItem('token', user.Authorization);
                        this.props.history.push('profile');
                        return "validUser";
                    } else {
                        localStorage.removeItem('userId');
                        localStorage.removeItem('token');
                        return "invalidUser"
                    }

                },
                userProfile: async (params, headers) => {
                    const data = await userAuth(params, headers);
                    this.setState({ user: data })
                }
            }}>
                {this.props.children}
            </myContext.Provider>
        )
    }
}
export default withRouter(Provider);

