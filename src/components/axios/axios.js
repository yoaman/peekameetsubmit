import {baseUrl,loginUrl,UserDetails,getNote,addNote,deleteNote,editNote} from './constant';
import Axios from 'axios';
import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

export const userLogin=async (state)=>(await Axios.post(baseUrl+loginUrl,state).then((Response)=>{
        const data=Response.data;
        const userId=data.data[0].customer._id;
        const Authorization=data.data[0].token;
        return {userId,Authorization};
}).catch(Response=>{
        toast.notify('Invalid Email or Password')
}));
export const userAuth=
async (params,headers)=>(await Axios.get(baseUrl+UserDetails,{params,headers}).then((Response)=>{
       return (Response.data.data[0])
        
}))
export const getNotes=(data)=>{
        return Axios.get(baseUrl+getNote,data)
}
export const addNotes=(body,headers)=>{
        console.log("addNotes")
        console.log(baseUrl+addNote,body,headers);
        return Axios.post(baseUrl+addNote,body,headers)
      }
export const deleteNotes=(data)=>{
        console.log("deletef note data",data)
        return Axios.delete(baseUrl+deleteNote+data.params.noteId,data)
        }
export const editNotes=(noteId,body,headers)=>{
        return Axios.put(baseUrl+editNote+noteId,body,headers);
}